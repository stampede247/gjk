This program was made by Taylor Robbins for Math 208 at Bellevue College on Nov. 9th 2018
The source code can be found at https://www.bitbucket.org/stampede247/gjk/
Any questions can be sent to robbitay@gmail.com

Basic Controls:
	WASD: Move camera
	Left Click: Move Square
	Right Click: Move Circle
	Shift + Left Click: Move polygon point (movable vertex changes after every release of right click)

Togglables:
	Tilde (~): Grid snapping
	1: Bezier Curves (Flowers)
	2: Circle-Rectangle Addition/Subtraction shapes
	3: Rectangle-Polygon Addition/Subtraction shapes
	4: Circle-Polygon Addition/Subtraction shapes

GJK: (Only operates on Circle vs Rec, Recommended to turn on Circle-Rectangle Addition/Subtraction shapes: Press 2)
	Enter: GJK Step (Resets if finished)
	Backspace: Stop GJK Algorithm
	Z (Hold): Constant GJK step




