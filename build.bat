@echo off

echo Running on %ComputerName%
IF EXIST "build*" (
	echo Moving into build folder
	cd build
)

python --version 2>NUL
if errorlevel 1 (
	echo Python isn't installed on this computer
	set PythonInstalled=0
) else (
	set PythonInstalled=1
)

call "C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat" x64

set TimeString=%date:~-4,4%%date:~-10,2%%date:~-7,2%%time:~0,2%%time:~3,2%%time:~6,2%

rem mkdir build > NUL 2> NUL
rem echo Running from %cd%
rem echo Time is %TimeString%

set ProjectName=GJK
set LibDirectory=..\..\..\lib
set SourceDirectory=..\code
set DataDirectory=..\data

set CompilePlatform=1
set CompileApplication=1
set DebugBuild=0
set CopyToDataDirectory=1

if "%DebugBuild%"=="1" (
	set DebugDependantFlags=/MTd /Od -DDEBUG=1
	set DebugDependantPaths=/LIBPATH:"..\lib\Win32\Debug"
	set DebugDependantLibraries=glew32d.lib
) else (
	set DebugDependantFlags=/MT /O2 /Ot /Oy -DDEBUG=0
	set DebugDependantPaths=/LIBPATH:"..\lib\Win32\Release"
	set DebugDependantLibraries=glew32.lib
)

set CompilerFlags=%DebugDependantFlags% -DWINDOWS_COMPILATION -DPROJECT_NAME=\"%ProjectName%\" /FC /Zi /EHsc /nologo /GS- /Gm- -GR- /EHa- /Fm /Oi /WX /W4 /wd4201 /wd4100 /wd4189 /wd4996 /wd4127 /wd4505 /wd4101 /wd4702 /wd4458 /wd4324
set LinkerFlags=-incremental:no
set IncludeDirectories=/I"%SourceDirectory%" /I"%LibDirectory%\mylib" /I"%LibDirectory%\glew-2.0.0\include" /I"%LibDirectory%\glfw-3.2.1\include" /I"%LibDirectory%\stb"
set LibraryDirectories=%DebugDependantPaths%
set Libraries=gdi32.lib User32.lib Shell32.lib opengl32.lib glfw3.lib Shlwapi.lib %DebugDependantLibraries%
set AppExports=/EXPORT:App_GetVersion /EXPORT:App_Initialize /EXPORT:App_Reloading /EXPORT:App_Reloaded /EXPORT:App_Update /EXPORT:App_Closing

rem echo [Building...]

REM Compile the resources file to generate resources.res which defines our program icon
rc /nologo %SourceDirectory%\resources.rc

del *.pdb > NUL 2> NUL

if "%CompilePlatform%"=="1" (
	echo[
	
	if "%PythonInstalled%"=="1" (
		python ..\IncrementVersionNumber.py %SourceDirectory%\win32\win32_version.h
	)
	
	cl /Fe%ProjectName%.exe %CompilerFlags% %IncludeDirectories% %SourceDirectory%\win32\win32_main.cpp /link %LibraryDirectories% %LinkerFlags% %Libraries% kernel32.lib %SourceDirectory%\resources.res
	
	if "%ERRORLEVEL%"=="0" (
		if "%CopyToDataDirectory%"=="1" (
			echo [Copying %ProjectName%.exe to data directory]
			XCOPY ".\%ProjectName%.exe" "%DataDirectory%\" /Y > NUL
		) else (
			echo [Platform Build Succeeded!]
		)
	) else (
		echo [Platform Build Failed! %ERRORLEVEL%]
	)
)

if "%CompileApplication%"=="1" (
	echo[
	
	if "%PythonInstalled%"=="1" (
		python ..\IncrementVersionNumber.py %SourceDirectory%\app\app_version.h
	)
	
	cl /Fe%ProjectName%.dll %CompilerFlags% %IncludeDirectories% %SourceDirectory%\app\app.cpp /link %LibraryDirectories% %LinkerFlags% %Libraries% %AppExports% /DLL /PDB:"%ProjectName%_%TimeString%.pdb"
	
	if "%ERRORLEVEL%"=="0" (
		if "%CopyToDataDirectory%"=="1" (
			echo [Copying %ProjectName%.dll to data directory]
			XCOPY ".\%ProjectName%.dll" "%DataDirectory%\" /Y > NUL
		) else (
			echo [Application Build Succeeded!]
		)
	) else (
		echo [Application Build Failed! %ERRORLEVEL%]
	)
)

echo[

REM Delete the object files that were created during compilation
del *.obj > NUL 2> NUL

rem echo [Done!]