/*
File:   implementation.cpp
Author: Taylor Robbins
Date:   11\09\2018
Description: 
	** This is a 3D implementation test of GJK that you can manipulate and test that the algorithm works
*/

// +--------------------------------------------------------------+
// |              Initialize Implementation AppState              |
// +--------------------------------------------------------------+
void InitializeImplementationState()
{
	Assert(imp != nullptr);
	
	//TODO: Allocate stuff
	
	imp->initialized = true;
}

// +--------------------------------------------------------------+
// |                Start Implementation AppState                 |
// +--------------------------------------------------------------+
void StartImplementationState(AppState_t oldState)
{
	Assert(imp != nullptr && imp->initialized);
	
	//TODO: Resume the state
}

// +--------------------------------------------------------------+
// |             Deinitialize Implementation AppState             |
// +--------------------------------------------------------------+
void DeinitializeImplementationState()
{
	//TODO: Deallocate stuff
	
	ClearPointer(imp);
}

// +--------------------------------------------------------------+
// |                Update Implementation AppState                |
// +--------------------------------------------------------------+
AppState_t UpdateImplementationState()
{
	AppState_t newState = AppState_Implementation;
	
	// +--------------------------------------------------------------+
	// |                            Update                            |
	// +--------------------------------------------------------------+
	{
		
	}
	
	// +--------------------------------------------------------------+
	// |                            Render                            |
	// +--------------------------------------------------------------+
	{
		RcBegin(&app->defaultShader, &app->defaultFont, NewRec(Vec2_Zero, RenderScreenSize));
		RcClearColorBuffer(NewColor(Color_Red));
		RcClearDepthBuffer(1.0f);
		RcBindNewFont(&app->newFont);
		RcSetFontSize(24);
		
		
	}
	
	return newState;
}

