/*
File:   app_functions.cpp
Author: Taylor Robbins
Date:   11\09\2018
Description: 
	** Holds functions that are used by app.cpp to perform it's functions 
*/

void AppEntryPoint(const PlatformInfo_t* PlatformInfo, const AppMemory_t* AppMemory, const AppInput_t* AppInput = nullptr, AppOutput_t* AppOutput = nullptr)
{
	platform = PlatformInfo;
	input = AppInput;
	appOutput = AppOutput;
	
	app = (AppData_t*)AppMemory->permanantPntr;
	if (app != nullptr)
	{
		mainHeap = &app->mainHeap;
		TempArena = &app->tempArena;
		renderContext = &app->renderContext;
		test = &app->testerData;
		imp = &app->implementationData;
	}
	else
	{
		mainHeap = nullptr;
		TempArena = nullptr;
		renderContext = nullptr;
		test = nullptr;
		imp = nullptr;
	}
	
	if (platform != nullptr)
	{
		RenderScreenSize = NewVec2(platform->screenSize);
	}
	if (input != nullptr)
	{
		RenderMousePos = input->mousePos;
		RenderMouseStartLeft = input->mouseStartPos[MouseButton_Left];
		RenderMouseStartRight = input->mouseStartPos[MouseButton_Right];
	}
}

void InitializeAppState(AppState_t state)
{
	DEBUG_PrintLine("[Initializing AppState_%s]", GetAppStateStr(state));
	switch (state)
	{
		case AppState_Tester:         InitializeTesterState(); break;
		case AppState_Implementation: InitializeImplementationState(); break;
		default: DEBUG_WriteLine("WARNING: Invalid appState init"); break;
	};
}

void StartAppState(AppState_t state, AppState_t oldState)
{
	DEBUG_PrintLine("[Starting AppState_%s]", GetAppStateStr(state));
	switch (state)
	{
		case AppState_Tester:         StartTesterState(oldState); break;
		case AppState_Implementation: StartImplementationState(oldState); break;
		default: DEBUG_WriteLine("WARNING: Invalid appState start"); break;
	};
}

void DeinitializeAppState(AppState_t state)
{
	DEBUG_PrintLine("[Deinitializing AppState_%s]", GetAppStateStr(state));
	switch (state)
	{
		case AppState_Tester:         DeinitializeTesterState(); break;
		case AppState_Implementation: DeinitializeImplementationState(); break;
		default: DEBUG_WriteLine("WARNING: Invalid appState start"); break;
	};
}

AppState_t UpdateAppState(AppState_t state)
{
	switch (state)
	{
		case AppState_Tester:         return UpdateTesterState();
		case AppState_Implementation: return UpdateImplementationState();
		default: DEBUG_WriteLine("WARNING: Invalid appState start"); return state;
	};
}

