/*
File:   app_helpers.cpp
Author: Taylor Robbins
Date:   03\03\2018
Description: 
	** Holds some helper functions that all of the AppStates have access to
*/

void AppChangeState(AppState_t newState, bool skipInitialization = false, bool skipDeinitialization = false)
{
	app->newAppState = newState;
	app->skipInitialization = skipInitialization;
	app->skipDeinitialization = skipDeinitialization;
}

#define DEBUG_Write(formatStr) do {      \
	if (platform != nullptr &&           \
		platform->DebugWrite != nullptr) \
	{                                    \
		platform->DebugWrite(formatStr); \
	}                                    \
} while (0)

#define DEBUG_WriteLine(formatStr) do {      \
	if (platform != nullptr &&               \
		platform->DebugWriteLine != nullptr) \
	{                                        \
		platform->DebugWriteLine(formatStr); \
	}                                        \
} while (0)

#define DEBUG_Print(formatStr, ...) do {              \
	if (platform != nullptr &&                        \
		platform->DebugPrint != nullptr)              \
	{                                                 \
		platform->DebugPrint(formatStr, __VA_ARGS__); \
	}                                                 \
} while (0)

#define DEBUG_PrintLine(formatStr, ...) do {              \
	if (platform != nullptr &&                            \
		platform->DebugPrintLine != nullptr)              \
	{                                                     \
		platform->DebugPrintLine(formatStr, __VA_ARGS__); \
	}                                                     \
} while (0)

#define HandleButton(button) do { app->buttonHandled[button] = true; } while(0)
#define IsButtonHandled(button) app->buttonHandled[button]

#define ButtonPressed(button) ((input->buttons[button].isDown && input->buttons[button].transCount > 0) || input->buttons[button].transCount >= 2)
#define ButtonPressedUnhandled(button) (app->buttonHandled[button] == false && ((input->buttons[button].isDown && input->buttons[button].transCount > 0) || input->buttons[button].transCount >= 2))
#define ButtonReleased(button) ((!input->buttons[button].isDown && input->buttons[button].transCount > 0) || input->buttons[button].transCount >= 2)
#define ButtonReleasedUnhandled(button) (app->buttonHandled[button] == false && ((!input->buttons[button].isDown && input->buttons[button].transCount > 0) || input->buttons[button].transCount >= 2))
#define ButtonDown(button) (input->buttons[button].isDown)
#define ButtonDownUnhandled(button) (app->buttonHandled[button] == false && input->buttons[button].isDown)

#define ClickedOnRec(rectangle) (ButtonReleasedUnhandled(MouseButton_Left) && IsInsideRec(rectangle, RenderMousePos) && IsInsideRec(rectangle, RenderMouseStartPos))
