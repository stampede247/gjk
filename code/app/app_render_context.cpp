/*
File:   app_render_context.cpp
Author: Taylor Robbins
Date:   03\03\2018
Description: 
	** The renderContext is a helpful wrapper of most OpenGL calls into an interface
	** that allows us to draw sprites, rectangles, text, and other primitives easily
*/

void InitializeRenderContext()
{
	Assert(renderContext != nullptr);
	ClearPointer(renderContext);
	
	Vertex_t squareVertices[] =
	{
		{  {0.0f, 0.0f, 0.0f}, NewVec4(NewColor(Color_White)), {0.0f, 0.0f} },
		{  {1.0f, 0.0f, 0.0f}, NewVec4(NewColor(Color_White)), {1.0f, 0.0f} },
		{  {0.0f, 1.0f, 0.0f}, NewVec4(NewColor(Color_White)), {0.0f, 1.0f} },
		
		{  {0.0f, 1.0f, 0.0f}, NewVec4(NewColor(Color_White)), {0.0f, 1.0f} },
		{  {1.0f, 0.0f, 0.0f}, NewVec4(NewColor(Color_White)), {1.0f, 0.0f} },
		{  {1.0f, 1.0f, 0.0f}, NewVec4(NewColor(Color_White)), {1.0f, 1.0f} },
	};
	renderContext->squareBuffer = CreateVertexBuffer(squareVertices, ArrayCount(squareVertices));
	
	renderContext->gradientTexture = LoadTexture("Resources/Textures/gradient.png", false, false);
	renderContext->circleTexture = LoadTexture("Resources/Sprites/circle.png", false, false);
	
	Color_t textureData = {Color_White};
	renderContext->dotTexture = CreateTexture((u8*)&textureData, 1, 1);
	
	renderContext->viewport = NewRec(0, 0, (r32)RenderScreenSize.x, (r32)RenderScreenSize.y);
	renderContext->worldMatrix = Mat4_Identity;
	renderContext->viewMatrix = Mat4_Identity;
	renderContext->projectionMatrix = Mat4_Identity;
	renderContext->doGrayscaleGradient = false;
	renderContext->useAlphaTexture = false;
	renderContext->sourceRectangle = NewRec(0, 0, 1, 1);
	renderContext->depth = 1.0f;
	renderContext->circleRadius = 0.0f;
	renderContext->circleInnerRadius = 0.0f;
	renderContext->vigRadius = 0.0f;
	renderContext->vigSmoothness = 0.0f;
	renderContext->color = NewColor(Color_White);
	renderContext->secondaryColor = NewColor(Color_White);
}


// +--------------------------------------------------------------+
// |                    State Change Functions                    |
// +--------------------------------------------------------------+
void RcUpdateShader()
{
	Assert(renderContext->boundShader != nullptr);
	
	if (renderContext->boundBuffer != nullptr)
	{
		glBindVertexArray(renderContext->boundShader->vertexArray);
		glBindBuffer(GL_ARRAY_BUFFER, renderContext->boundBuffer->id);
		glVertexAttribPointer(renderContext->boundShader->locations.positionAttrib, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex_t), (void*)0);
		glVertexAttribPointer(renderContext->boundShader->locations.colorAttrib,    4, GL_FLOAT, GL_FALSE, sizeof(Vertex_t), (void*)sizeof(v3));
		glVertexAttribPointer(renderContext->boundShader->locations.texCoordAttrib, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex_t), (void*)(sizeof(v3)+sizeof(v4)));
	}
	
	if (renderContext->boundTexture != nullptr)
	{
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, renderContext->boundTexture->id);
		glUniform1i(renderContext->boundShader->locations.diffuseTexture, 0);
		glUniform2f(renderContext->boundShader->locations.textureSize, (r32)renderContext->boundTexture->width, (r32)renderContext->boundTexture->height);
	}
	
	if (renderContext->boundAlphaTexture != nullptr)
	{
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, renderContext->boundAlphaTexture->id);
		glUniform1i(renderContext->boundShader->locations.alphaTexture, 1);
		glUniform1i(renderContext->boundShader->locations.useAlphaTexture, 1);
	}
	else
	{
		glUniform1i(renderContext->boundShader->locations.useAlphaTexture, 0);
	}
	
	if (renderContext->boundFrameBuffer != nullptr)
	{
		glBindFramebuffer(GL_FRAMEBUFFER, renderContext->boundFrameBuffer->id);
	}
	else
	{
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}
	
	glUniformMatrix4fv(renderContext->boundShader->locations.worldMatrix,      1, GL_FALSE, &renderContext->worldMatrix.values[0][0]);
	glUniformMatrix4fv(renderContext->boundShader->locations.viewMatrix,       1, GL_FALSE, &renderContext->viewMatrix.values[0][0]);
	glUniformMatrix4fv(renderContext->boundShader->locations.projectionMatrix, 1, GL_FALSE, &renderContext->projectionMatrix.values[0][0]);
	
	glUniform1i(renderContext->boundShader->locations.doGrayscaleGradient, renderContext->doGrayscaleGradient ? 1 : 0);
	glUniform4f(renderContext->boundShader->locations.sourceRectangle, renderContext->sourceRectangle.x, renderContext->sourceRectangle.y, renderContext->sourceRectangle.width, renderContext->sourceRectangle.height);
	glUniform1f(renderContext->boundShader->locations.circleRadius, renderContext->circleRadius);
	glUniform1f(renderContext->boundShader->locations.circleInnerRadius, renderContext->circleInnerRadius);
	glUniform2f(renderContext->boundShader->locations.vignette, renderContext->vigRadius, renderContext->vigSmoothness);
	
	v4 colorVec = NewVec4(renderContext->color);
	glUniform4f(renderContext->boundShader->locations.diffuseColor, colorVec.r, colorVec.g, colorVec.b, colorVec.a);
	colorVec = NewVec4(renderContext->secondaryColor);
	glUniform4f(renderContext->boundShader->locations.secondaryColor, colorVec.r, colorVec.g, colorVec.b, colorVec.a);
}

void RcBindShader(const Shader_t* shaderPntr)
{
	renderContext->boundShader = shaderPntr;
	
	glUseProgram(shaderPntr->programId);
}

void RcBindFont(const Font_t* fontPntr)
{
	renderContext->boundFont = fontPntr;
}

void RcBindNewFont(NewFont_t* fontPntr)
{
	renderContext->boundNewFont = fontPntr;
}

void RcSetFontSize(r32 fontSize)
{
	renderContext->fontSize = fontSize;
}

void RcSetFontStyle(u16 fontStyle)
{
	renderContext->fontStyle = fontStyle;
}

void RcSetFontAlignment(Alignment_t alignment)
{
	renderContext->fontAlignment = alignment;
}

void RcBindTexture(const Texture_t* texturePntr)
{
	renderContext->boundTexture = texturePntr;
	
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, renderContext->boundTexture->id);
	glUniform1i(renderContext->boundShader->locations.diffuseTexture, 0);
	glUniform2f(renderContext->boundShader->locations.textureSize, (r32)renderContext->boundTexture->width, (r32)renderContext->boundTexture->height);
}

void RcBindAlphaTexture(const Texture_t* texturePntr)
{
	renderContext->boundAlphaTexture = texturePntr;
	
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, renderContext->boundAlphaTexture->id);
	glUniform1i(renderContext->boundShader->locations.alphaTexture, 1);
	glUniform1i(renderContext->boundShader->locations.useAlphaTexture, 1);
}
void RcDisableAlphaTexture()
{
	renderContext->boundAlphaTexture = nullptr;
	
	glUniform1i(renderContext->boundShader->locations.useAlphaTexture, 0);
}

void RcBindBuffer(const VertexBuffer_t* vertBufferPntr)
{
	renderContext->boundBuffer = vertBufferPntr;
	
	glBindVertexArray(renderContext->boundShader->vertexArray);
	glBindBuffer(GL_ARRAY_BUFFER, vertBufferPntr->id);
	glVertexAttribPointer(renderContext->boundShader->locations.positionAttrib, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex_t), (void*)0);
	glVertexAttribPointer(renderContext->boundShader->locations.colorAttrib,    4, GL_FLOAT, GL_FALSE, sizeof(Vertex_t), (void*)sizeof(v3));
	glVertexAttribPointer(renderContext->boundShader->locations.texCoordAttrib, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex_t), (void*)(sizeof(v3)+sizeof(v4)));
}

void RcBindFrameBuffer(const FrameBuffer_t* frameBuffer)
{
	renderContext->boundFrameBuffer = frameBuffer;
	
	if (frameBuffer == nullptr)
	{
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}
	else
	{
		glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer->id);
	}
}

void RcSetWorldMatrix(const mat4& worldMatrix)
{
	renderContext->worldMatrix = worldMatrix;
	glUniformMatrix4fv(renderContext->boundShader->locations.worldMatrix, 1, GL_FALSE, &worldMatrix.values[0][0]);
}
void RcSetViewMatrix(const mat4& viewMatrix)
{
	renderContext->viewMatrix = viewMatrix;
	glUniformMatrix4fv(renderContext->boundShader->locations.viewMatrix, 1, GL_FALSE, &viewMatrix.values[0][0]);
}
void RcSetProjectionMatrix(const mat4& projectionMatrix)
{
	renderContext->projectionMatrix = projectionMatrix;
	glUniformMatrix4fv(renderContext->boundShader->locations.projectionMatrix, 1, GL_FALSE, &projectionMatrix.values[0][0]);
}

void RcSetViewport(rec viewport)
{
	renderContext->viewport = viewport;
	
	#if DOUBLE_RESOLUTION
	glViewport(
		(i32)renderContext->viewport.x*2, 
		(i32)(RenderScreenSize.y*2 - renderContext->viewport.height*2 - renderContext->viewport.y*2), 
		(i32)renderContext->viewport.width*2, 
		(i32)renderContext->viewport.height*2
	);
	#else
	glViewport(
		(i32)renderContext->viewport.x, 
		(i32)(RenderScreenSize.y - renderContext->viewport.height - renderContext->viewport.y), 
		(i32)renderContext->viewport.width, 
		(i32)renderContext->viewport.height
	);
	#endif
	
	mat4 projMatrix;
	projMatrix = Mat4Scale(NewVec3(2.0f/viewport.width, -2.0f/viewport.height, 1.0f));
	projMatrix = Mat4Multiply(projMatrix, Mat4Translate(NewVec3(-viewport.width/2.0f, -viewport.height/2.0f, 0.0f)));
	projMatrix = Mat4Multiply(projMatrix, Mat4Translate(NewVec3(-renderContext->viewport.x, -renderContext->viewport.y, 0.0f)));
	RcSetProjectionMatrix(projMatrix);
}

void RcSetColor(Color_t color)
{
	renderContext->color = color;
	
	v4 colorVec = NewVec4(color);
	glUniform4f(renderContext->boundShader->locations.diffuseColor, colorVec.r, colorVec.g, colorVec.b, colorVec.a);
}
void RcSetSecondaryColor(Color_t color)
{
	renderContext->secondaryColor = color;
	
	v4 colorVec = NewVec4(color);
	glUniform4f(renderContext->boundShader->locations.secondaryColor, colorVec.r, colorVec.g, colorVec.b, colorVec.a);
}

void RcSetSourceRectangle(rec sourceRectangle)
{
	renderContext->sourceRectangle = sourceRectangle;
	
	glUniform4f(renderContext->boundShader->locations.sourceRectangle, sourceRectangle.x, sourceRectangle.y, sourceRectangle.width, sourceRectangle.height);
}

void RcSetGradientEnabled(bool doGradient)
{
	renderContext->doGrayscaleGradient = doGradient;
	
	glUniform1i(renderContext->boundShader->locations.doGrayscaleGradient, doGradient ? 1 : 0);
}

void RcSetCircleRadius(r32 radius, r32 innerRadius = 0.0f)
{
	renderContext->circleRadius = radius;
	renderContext->circleInnerRadius = innerRadius;
	
	glUniform1f(renderContext->boundShader->locations.circleRadius, radius);
	glUniform1f(renderContext->boundShader->locations.circleInnerRadius, innerRadius);
}

void RcEnableVignette(r32 radius, r32 smoothness)
{
	renderContext->vigRadius = radius;
	renderContext->vigSmoothness = smoothness;
	
	glUniform2f(renderContext->boundShader->locations.vignette, radius, smoothness);
}

void RcDisableVignette()
{
	RcEnableVignette(0, 0);
}

void RcSetDepth(r32 depth)
{
	renderContext->depth = depth;
}

void RcBegin(const Shader_t* startShader, const Font_t* startFont, rec viewport)
{
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_BLEND);
	
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	
	glEnable(GL_ALPHA_TEST);
	glAlphaFunc(GL_GEQUAL, 0.1f);
	
	glDisable(GL_CULL_FACE);
	// glCullFace(GL_FRONT);
	
	RcBindShader(startShader);
	RcBindFont(startFont);
	RcBindTexture(&renderContext->dotTexture);
	RcBindBuffer(&renderContext->squareBuffer);
	RcBindFrameBuffer(nullptr);
	RcDisableAlphaTexture();
	
	RcSetWorldMatrix(Matrix4_Identity);
	RcSetViewMatrix(Matrix4_Identity);
	RcSetProjectionMatrix(Matrix4_Identity);
	RcSetViewport(viewport);
	RcSetColor(NewColor(Color_White));
	RcSetSecondaryColor(NewColor(Color_White));
	RcSetSourceRectangle(NewRec(0, 0, 1, 1));
	RcSetGradientEnabled(false);
	RcSetCircleRadius(0.0f, 0.0f);
	RcDisableVignette();
	RcSetDepth(1.0f);
}

void RcClearColorBuffer(Color_t clearColor)
{
	v4 clearColorVec = NewVec4(clearColor);
	glClearColor(clearColorVec.x, clearColorVec.y, clearColorVec.z, clearColorVec.w);
	glClear(GL_COLOR_BUFFER_BIT);
}
void RcClearDepthBuffer(r32 clearDepth)
{
	glClearDepth(clearDepth);
	glClear(GL_DEPTH_BUFFER_BIT);
}
