/*
File:   app.cpp
Author: Taylor Robbins
Date:   03\02\2018
Description: 
	** Holds the top-level functions that are exported in the application DLL that gets loaded by the platform layer
	** This is the only file that really gets compiled for the application. All other files are included inside this one
*/

#define RESET_APPLICATION false
#define USE_ASSERT_FAILURE_FUNCTION true

#include "platform/plat_interface.h"
#include "app/app_version.h"
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#define STB_TRUETYPE_IMPLEMENTATION
#include "stb_truetype.h"

#include "my_tempMemory.h"
#include "my_tempMemory.cpp"
#include "my_tempList.h"

// +--------------------------------------------------------------+
// |                   Application Header Files                   |
// +--------------------------------------------------------------+
#include "app/app_defines.h"
#include "app/app_structs.h"
#include "app/app_render_context.h"
#include "gjk/gjk.h"

#include "tester/tester.h"
#include "implementation/implementation.h"
#include "app/app.h"

// +--------------------------------------------------------------+
// |                     Application Globals                      |
// +--------------------------------------------------------------+
const PlatformInfo_t* platform = nullptr;
const AppInput_t* input = nullptr;
AppOutput_t* appOutput = nullptr;

AppData_t* app = nullptr;
TesterData_t* test = nullptr;
ImplementationData_t* imp = nullptr;

MemoryArena_t* mainHeap = nullptr;
RenderContext_t* renderContext = nullptr;
v2 RenderScreenSize = Vec2_Zero;
v2 RenderMousePos = Vec2_Zero;
v2 RenderMouseStartLeft = Vec2_Zero;
v2 RenderMouseStartRight = Vec2_Zero;

// +--------------------------------------------------------------+
// |                   Application Source Files                   |
// +--------------------------------------------------------------+
#include "app/app_helpers.cpp"
#include "app/app_performance.cpp"
#include "app/app_triangulation.cpp"
#include "app/app_texture.cpp"
#include "app/app_font.cpp"
#include "app/app_loadingFunctions.cpp"
#include "app/app_fontHelpers.cpp"
#include "app/app_render_context.cpp"
#include "app/app_render_functions.cpp"
#include "app/app_fontFlow.cpp"

#include "gjk/gjk.cpp"

#include "tester/tester.cpp"
#include "implementation/implementation.cpp"

#include "app/app_functions.cpp"

// +--------------------------------------------------------------+
// |                    Local Helper Functions                    |
// +--------------------------------------------------------------+
void AppLoadContent(bool firstLoad)
{
	if (!firstLoad)
	{
		DestroyShader(&app->defaultShader);
		DestroyFont(&app->defaultFont);
		DestroyNewFont(&app->newFont);
		DestroyNewFont(&app->debugFont);
	}
	
	app->defaultShader = LoadShader(SHADERS_FOLDER "simple-vertex.glsl", SHADERS_FOLDER "simple-fragment.glsl");
	app->defaultFont   = LoadFont(FONTS_FOLDER "georgiab.ttf", 24, 256, 256, ' ', 96);
	
	CreateGameFont(&app->newFont, mainHeap);
	FontLoadFile(&app->newFont, FONTS_FOLDER "georgiab.ttf", FontStyle_Bold);
	FontLoadFile(&app->newFont, FONTS_FOLDER "georgiai.ttf", FontStyle_Italic);
	FontLoadFile(&app->newFont, FONTS_FOLDER "georgiaz.ttf", FontStyle_BoldItalic);
	FontLoadFile(&app->newFont, FONTS_FOLDER "georgia.ttf",  FontStyle_Default);
	FontBake(&app->newFont, 12);
	FontBake(&app->newFont, 24);
	FontBake(&app->newFont, 32);
	FontBake(&app->newFont, 32, FontStyle_Bold);
	FontBake(&app->newFont, 32, FontStyle_Italic);
	FontBake(&app->newFont, 32, FontStyle_BoldItalic);
	FontDropFiles(&app->newFont);
	
	CreateGameFont(&app->debugFont, mainHeap);
	FontLoadFile(&app->debugFont, FONTS_FOLDER "consola.ttf", FontStyle_Default);
	FontBake(&app->debugFont, 12);
	FontBake(&app->debugFont, 16);
	FontBake(&app->debugFont, 18);
	FontBake(&app->debugFont, 20);
	FontBake(&app->debugFont, 24);
	FontDropFiles(&app->debugFont);
}

// +--------------------------------------------------------------+
// |                       App Get Version                        |
// +--------------------------------------------------------------+
// Version_t App_GetVersion(bool* resetApplication)
EXPORT AppGetVersion_DEFINITION(App_GetVersion)
{
	Version_t version = { APP_VERSION_MAJOR, APP_VERSION_MINOR, APP_VERSION_BUILD };
	if (resetApplication != nullptr) { *resetApplication = RESET_APPLICATION; }
	return version;
}

// +--------------------------------------------------------------+
// |                        App Initialize                        |
// +--------------------------------------------------------------+
// void App_Initialize(const PlatformInfo_t* PlatformInfo, const AppMemory_t* AppMemory)
EXPORT AppInitialize_DEFINITION(App_Initialize)
{
	AppEntryPoint(PlatformInfo, AppMemory);
	
	DEBUG_WriteLine("Initializing application...");
	
	// +==============================+
	// |     Setup Memory Arenas      |
	// +==============================+
	{
		Assert(sizeof(AppData_t) < AppMemory->permanantSize);
		ClearPointer(app);
		
		u32 mainHeapSize = AppMemory->permanantSize - sizeof(AppData_t);
		InitializeMemoryArenaHeap(&app->mainHeap, (void*)(app + 1), mainHeapSize);
		InitializeMemoryArenaTemp(&app->tempArena, AppMemory->transientPntr, AppMemory->transientSize, TRANSIENT_MAX_NUMBER_MARKS);
		InitializeMemoryArenaStdHeap(&app->stdArena);
		
		DEBUG_PrintLine("Main Heap:  %u bytes", mainHeapSize);
		DEBUG_PrintLine("Temp Arena: %u bytes", AppMemory->transientSize);
	}
	
	TempPushMark();
	 
	// +==============================+
	// |      Initialize Things       |
	// +==============================+
	InitializeRenderContext();
	AppLoadContent(true);
	
	// +==============================+
	// | Initialize Starting AppState |
	// +==============================+
	app->appState = AppState_Tester;
	app->newAppState = app->appState;
	DEBUG_PrintLine("[Initializing AppState_%s]", GetAppStateStr(app->appState));
	InitializeAppState(app->appState);
	StartAppState(app->appState, AppState_None);
	
	DEBUG_WriteLine("Application initialization finished!");
	TempPopMark();
	app->appInitTempHighWaterMark = ArenaGetHighWaterMark(TempArena);
	ArenaResetHighWaterMark(TempArena);
}

// +--------------------------------------------------------------+
// |                          App Update                          |
// +--------------------------------------------------------------+
// void App_Update(const PlatformInfo_t* PlatformInfo, const AppMemory_t* AppMemory, const AppInput_t* AppInput, AppOutput_t* AppOutput)
EXPORT AppUpdate_DEFINITION(App_Update)
{
	AppEntryPoint(PlatformInfo, AppMemory, AppInput, AppOutput);
	StartTimeBlock("AppUpdate");
	TempPushMark();
	
	// +==============================+
	// |   Relaod Content Shortcut    |
	// +==============================+
	if (ButtonPressed(Button_F5))
	{
		DEBUG_WriteLine("Reloading app content");
		AppLoadContent(false);
	}
	#if DEBUG
	if (ButtonPressed(Button_F11))
	{
		app->showDebugMenu = !app->showDebugMenu;
	}
	#endif
	
	// +==============================+
	// |   Update Current AppState    |
	// +==============================+
	app->newAppState = UpdateAppState(app->appState);
	
	// +==============================+
	// |       Change AppStates       |
	// +==============================+
	if (app->newAppState != app->appState)
	{
		if (!app->skipDeinitialization)
		{
			DeinitializeAppState(app->appState);
		}
		AppState_t oldState = app->appState;
		app->appState = app->newAppState;
		if (!app->skipInitialization)
		{
			InitializeAppState(app->appState);
		}
		app->skipInitialization = false;
		app->skipDeinitialization = false;
		StartAppState(app->appState, oldState);
	}
	
	// +--------------------------------------------------------------+
	// |                     Render Debug Overlay                     |
	// +--------------------------------------------------------------+
	#if DEBUG
	if (app->showDebugMenu)
	{
		StartTimeBlock("Debug Overlay");
		// +==============================+
		// |      Render Time Blocks      |
		// +==============================+
		TimedBlockInfo_t* appUpdateBlock = GetTimedBlockInfoByName("AppUpdate");
		if (appUpdateBlock != nullptr)
		{
			rec appUpdateRec = NewRec(10, 10, 20, RenderScreenSize.height-20);
			
			RcDrawButton(RecInflate(appUpdateRec, 1), NewColor(Color_LightGrey), NewColor(Color_White));
			
			r32 blockOffset = 0;
			r32 keyOffset = 0;
			u32 bIndex = 0;
			RcBindNewFont(&app->debugFont);
			RcSetFontSize(16);
			RcSetFontAlignment(Alignment_Left);
			RcSetFontStyle(FontStyle_Default);
			
			while (TimedBlockInfo_t* blockInfo = GetTimedBlockInfoByParent("AppUpdate", bIndex))
			{
				r32 percent = (r32)blockInfo->counterElapsed / (r32)appUpdateBlock->counterElapsed;
				rec partRec = appUpdateRec;
				partRec.y += blockOffset;
				partRec.height *= percent;
				Color_t partColor = NewColor(GetColorByIndex(bIndex));
				RcDrawRectangle(partRec, partColor);
				// if (partRec.height >= RcGetLineHeight() || IsInsideRec(partRec, RenderMousePos))
				{
					rec keyColorRec = NewRec(appUpdateRec.topLeft + appUpdateRec.size, NewVec2(10));
					keyColorRec.x += 5;
					keyColorRec.y -= keyColorRec.height + 20 + keyOffset;
					v2 textPos = keyColorRec.topLeft + NewVec2(keyColorRec.width + 2, keyColorRec.height/2 - RcGetLineHeight()/2 + RcGetMaxExtendUp());
					RcDrawButton(keyColorRec, partColor, NewColor(Color_White));
					if (blockInfo->numCalls > 1)
					{
						RcNewPrintString(textPos, NewColor(Color_White), "%s x %u: %.1f%% %lu avg", blockInfo->blockName, blockInfo->numCalls, percent*100, blockInfo->counterElapsed / blockInfo->numCalls);
					}
					else
					{
						RcNewPrintString(textPos, NewColor(Color_White), "%s: %.1f%% %lu", blockInfo->blockName, percent*100, blockInfo->counterElapsed);
					}
					keyOffset += RcGetLineHeight();
				}
				blockOffset += partRec.height;
				bIndex++;
			}
			
			v2 lastTextPos = appUpdateRec.topLeft + appUpdateRec.size;
			lastTextPos.x += 5;
			lastTextPos.y -= 10 + 20 + keyOffset;
			RcNewPrintString(lastTextPos, NewColor(Color_White), "Total: %lu", appUpdateBlock->counterElapsed);
		}
		EndTimeBlock();
	}
	#endif
	
	TempPopMark();
	
	if (ButtonDown(Button_Space))
	{
		StartTimeBlock("Frame Flip");
		platform->FrameFlip();
		EndTimeBlock();
		
		EndTimeBlock();
	}
	else
	{
		EndTimeBlock();
		
		platform->FrameFlip();
	}
	
	static u8 countdown = 1;
	if (--countdown == 0) { countdown = 10; SaveTimeBlocks(); }
	else { ClearTimeBlocks(); }
}

// +--------------------------------------------------------------+
// |                        App Reloading                         |
// +--------------------------------------------------------------+
// void App_Reloading(const PlatformInfo_t* PlatformInfo, const AppMemory_t* AppMemory)
EXPORT AppReloading_DEFINITION(App_Reloading)
{
	AppEntryPoint(PlatformInfo, AppMemory);
	#if DEBUG
	app->lastNumTimedBlockInfos = 0;
	#endif
}

// +--------------------------------------------------------------+
// |                         App Reloaded                         |
// +--------------------------------------------------------------+
// void App_Reloaded(const PlatformInfo_t* PlatformInfo, const AppMemory_t* AppMemory)
EXPORT AppReloaded_DEFINITION(App_Reloaded)
{
	AppEntryPoint(PlatformInfo, AppMemory);
	// TODO: Reallocate stuff that was deallocated in App_Reloading
}

// +--------------------------------------------------------------+
// |                         App Closing                          |
// +--------------------------------------------------------------+
// void App_Closing(const PlatformInfo_t* PlatformInfo, const AppMemory_t* AppMemory)
EXPORT AppClosing_DEFINITION(App_Closing)
{
	AppEntryPoint(PlatformInfo, AppMemory);
	//TODO: Deallocate anything?
}

#if DEBUG
//This function is declared in my_assert.h and needs to be implemented by us for a debug build to compile successfully
void AssertFailure(const char* function, const char* filename, int lineNumber, const char* expressionStr)
{
	DEBUG_PrintLine("Assertion Failure! %s in \"%s\" line %d: (%s) is not true", function, GetFileNamePart(filename), lineNumber, expressionStr);
}
#endif
