/*
File:   gjk.cpp
Author: Taylor Robbins
Date:   11\09\2018
Description: 
	** Holds the functions that actually perform GJK related algorithms and tasks
*/

v2 GetCircleSupport(Circle_t circle, v2 dirVec)
{
	return circle.center + dirVec * circle.radius;
}

v2 GetRectSupport(rec rectangle, v2 dirVec)
{
	r32 topLeft     = Vec2Dot(rectangle.topLeft,                                              dirVec);
	r32 topRight    = Vec2Dot(rectangle.topLeft + NewVec2(rectangle.width, 0),                dirVec);
	r32 bottomLeft  = Vec2Dot(rectangle.topLeft + NewVec2(0, rectangle.height),               dirVec);
	r32 bottomRight = Vec2Dot(rectangle.topLeft + NewVec2(rectangle.width, rectangle.height), dirVec);
	if (topLeft >= topRight && topLeft >= bottomLeft && topLeft >= bottomRight)
	{
		return rectangle.topLeft + NewVec2(0, 0);
	}
	else if (topRight >= bottomLeft && topRight >= bottomRight)
	{
		return rectangle.topLeft + NewVec2(rectangle.width, 0);
	}
	else if (bottomLeft >= bottomRight)
	{
		return rectangle.topLeft + NewVec2(0, rectangle.height);
	}
	else
	{
		return rectangle.topLeft + NewVec2(rectangle.width, rectangle.height);
	}
}

v2 GetPolyCenter(const Polygon_t* polygon)
{
	Assert(polygon->numVerts > 0);
	v2 result = Vec2_Zero;
	for (u32 vIndex = 0; vIndex < polygon->numVerts; vIndex++)
	{
		result += polygon->verts[vIndex];
	}
	return result / (r32)polygon->numVerts;
}

v2 GetPolySupport(const Polygon_t* polygon, v2 dirVec)
{
	Assert(polygon->numVerts > 0);
	
	v2 result = Vec2_Zero;
	r32 max = 0.0f;
	for (u32 vIndex = 0; vIndex < polygon->numVerts; vIndex++)
	{
		r32 dot = Vec2Dot(polygon->verts[vIndex], dirVec);
		if (vIndex == 0 || dot > max)
		{
			max = dot;
			result = polygon->verts[vIndex];
		}
	}
	
	return result;
}

typedef v2 GJK2D_Support_f(const void* shape1, const void* shape2, v2 dirVec);

//Is b in direction of origin from a
bool SameDirection(v2 a, v2 b, v2 c)
{
	return (Vec2Dot(b-a, c-a) >= 0);
}

v2 PerpAwayFrom(v2 lineStart, v2 lineEnd, v2 awayFrom)
{
	v2 result = Vec2PerpRight(lineEnd - lineStart);
	if (Vec2Dot(result, awayFrom - lineStart) >= 0)
	{
		result = -result;
	}
	return result;
}

bool GJK2D_DoSimplex(GJK2D_t* gjk)
{
	Assert(gjk->numVerts >= 2 && gjk->numVerts <= 3);
	if (gjk->numVerts == 2)
	{
		if (SameDirection(gjk->verts[1], gjk->verts[0], Vec2_Zero))
		{
			gjk->dirVec = Vec2Normalize(Vec2PerpRight(gjk->verts[1] - gjk->verts[0]));
			if (!SameDirection(gjk->verts[1], gjk->verts[1] + gjk->dirVec, Vec2_Zero))
			{
				gjk->dirVec = -gjk->dirVec;
			}
			return false;
		}
		else
		{
			gjk->verts[0] = gjk->verts[1];
			gjk->numVerts = 1;
			gjk->dirVec = -Vec2Normalize(gjk->verts[0]);
			return false;
		}
	}
	else //3 points
	{
		// if (IsInsideTriangle(Vec2_Zero, gjk->verts[0], gjk->verts[1], gjk->verts[2])) { return true; }
		v2 perp1 = PerpAwayFrom(gjk->verts[2], gjk->verts[1], gjk->verts[0]);
		v2 perp2 = PerpAwayFrom(gjk->verts[2], gjk->verts[0], gjk->verts[1]);
		if (SameDirection(gjk->verts[2], gjk->verts[2] + perp2, Vec2_Zero))
		{
			if (SameDirection(gjk->verts[2], gjk->verts[0], Vec2_Zero))
			{
				gjk->verts[1] = gjk->verts[2];
				gjk->numVerts = 2;
				gjk->dirVec = Vec2Normalize(perp2);
				return false;
			}
			else
			{
				gjk->numVerts = 1;
				gjk->verts[0] = gjk->verts[2];
				gjk->dirVec = -Vec2Normalize(gjk->verts[0]);
				return false;
			}
		}
		else if (SameDirection(gjk->verts[2], gjk->verts[2] + perp1, Vec2_Zero))
		{
			if (SameDirection(gjk->verts[2], gjk->verts[1], Vec2_Zero))
			{
				gjk->verts[0] = gjk->verts[1];
				gjk->verts[1] = gjk->verts[2];
				gjk->numVerts = 2;
				gjk->dirVec = Vec2Normalize(perp1);
				return false;
			}
			else
			{
				gjk->numVerts = 1;
				gjk->verts[0] = gjk->verts[2];
				gjk->dirVec = -Vec2Normalize(gjk->verts[0]);
				return false;
			}
		}
		else
		{
			return true;
		}
	}
}

i32 GJK2D_Step(GJK2D_t* gjk, GJK2D_Support_f* supportFunc, const void* shape1, const void* shape2)
{
	if (gjk->numVerts == 0)
	{
		gjk->verts[0] = supportFunc(shape1, shape2, Vec2_Right);
		gjk->numVerts = 1;
		gjk->dirVec = -Vec2Normalize(gjk->verts[0]);
		return 0;
	}
	
	// Assert(gjk->numVerts < 3);
	if (gjk->numVerts >= 3) { gjk->numVerts = 2; }
	v2 newPoint = supportFunc(shape1, shape2, gjk->dirVec);
	gjk->verts[gjk->numVerts] = newPoint;
	gjk->numVerts++;
	if (Vec2Dot(newPoint, gjk->dirVec) < 0) { return -1; }
	if (GJK2D_DoSimplex(gjk)) { return 1; }
	return 0;
}

v2 GJK2D_SupportCircleRect(const void* shape1, const void* shape2, v2 dirVec)
{
	Circle_t circle = *(Circle_t*)shape1;
	rec rectangle = *(rec*)shape2;
	return GetCircleSupport(circle, dirVec) - GetRectSupport(rectangle, -dirVec);
}

bool GJKCircleVsRec(const Circle_t* circle, const rec* rectangle)
{
	GJK2D_t gjk = {};
	u32 step = 0;
	while (step < 10)
	{
		i32 result = GJK2D_Step(&gjk, GJK2D_SupportCircleRect, circle, rectangle);
		if (result < 0) { return false; }
		else if (result > 0) { return true; }
		step++;
	}
	DEBUG_WriteLine("GJK failed to find answer!");
	return false;
}