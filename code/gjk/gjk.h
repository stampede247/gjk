/*
File:   gjk.h
Author: Taylor Robbins
Date:   11\09\2018
*/

#ifndef _GJK_H
#define _GJK_H

struct GJK2D_t
{
	u8 numVerts;
	v2 verts[3];
	v2 dirVec;
};

struct Circle_t
{
	v2 center;
	r32 radius;
};

inline v2 RecCenter(rec rectangle)
{
	return rectangle.topLeft + rectangle.size/2.0f;
}

#endif //  _GJK_H
