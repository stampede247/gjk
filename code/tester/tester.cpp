/*
File:   tester.cpp
Author: Taylor Robbins
Date:   11\09\2018
Description: 
	** The Tester AppState is a dumping ground for code that needs to be tested seperately from the actually implementation AppState
*/

// +--------------------------------------------------------------+
// |                  Initialize Tester AppState                  |
// +--------------------------------------------------------------+
void InitializeTesterState()
{
	Assert(test != nullptr);
	
	test->circle.center = NewVec2(100, 100);
	test->rectangle = NewRec(200, 200, 0, 0);
	
	test->poly.numVerts = 5;
	test->poly.verts = PushArray(mainHeap, v2, test->poly.numVerts);
	test->poly.verts[0] = NewVec2(100, 100);
	test->poly.verts[1] = NewVec2(200, 110);
	test->poly.verts[2] = NewVec2(210, 200);
	test->poly.verts[3] = NewVec2(100, 200);
	test->poly.verts[4] = NewVec2(80, 150);
	
	test->vertIndex = 0;
	
	test->initialized = true;
}

// +--------------------------------------------------------------+
// |                    Start Tester AppState                     |
// +--------------------------------------------------------------+
void StartTesterState(AppState_t oldState)
{
	Assert(test != nullptr && test->initialized);
	
	//TODO: Resume the state
}

// +--------------------------------------------------------------+
// |                 Deinitialize Tester AppState                 |
// +--------------------------------------------------------------+
void DeinitializeTesterState()
{
	if (test->poly.verts != nullptr)
	{
		ArenaPop(mainHeap, test->poly.verts);
	}
	
	ClearPointer(test);
}

// +--------------------------------------------------------------+
// |                    Update Tester AppState                    |
// +--------------------------------------------------------------+
AppState_t UpdateTesterState()
{
	AppState_t newState = AppState_Tester;
	r32 gridSize = 50;
	
	// +--------------------------------------------------------------+
	// |                            Update                            |
	// +--------------------------------------------------------------+
	{
		test->circle.radius = 100.0f;
		test->rectangle.size = NewVec2(200, 200);
		
		// +==============================+
		// |        View Movement         |
		// +==============================+
		{
			r32 moveSpeed = 10;
			if (ButtonDown(Button_Shift)) { moveSpeed = 25; }
			if (ButtonDown(Button_Control)) { moveSpeed = 3; }
			if (ButtonDown(Button_W)) { test->viewPos.y -= moveSpeed; }
			if (ButtonDown(Button_A)) { test->viewPos.x -= moveSpeed; }
			if (ButtonDown(Button_S)) { test->viewPos.y += moveSpeed; }
			if (ButtonDown(Button_D)) { test->viewPos.x += moveSpeed; }
		}
		v2 mousePos = RenderMousePos + test->viewPos;
		v2 mouseGridPos = Vec2Round(mousePos / gridSize) * gridSize;
		v2 mouseSnapPos = (test->snapToGrid ? mouseGridPos : mousePos);
		
		if (ButtonDown(MouseButton_Left))
		{
			if (ButtonDown(Button_Shift))
			{
				test->poly.verts[test->vertIndex] = mouseSnapPos;
			}
			else
			{
				v2 newPos = mouseSnapPos - test->rectangle.size/2;
				if (newPos != test->rectangle.topLeft)
				{
					test->rectangle.topLeft = newPos;
					test->doingGJK2D = false;
					test->finishedGJK2D = false;
					test->foundOverlapGJK2D = false;
				}
			}
		}
		if (ButtonReleased(MouseButton_Left)) { test->vertIndex = ((test->vertIndex+1) % test->poly.numVerts); }
		if (ButtonDown(MouseButton_Right))
		{
			if (test->circle.center != mouseSnapPos)
			{
				test->circle.center = mouseSnapPos;
				test->doingGJK2D = false;
				test->finishedGJK2D = false;
				test->foundOverlapGJK2D = false;
			}
		}
		
		if (ButtonPressed(Button_Enter) || ButtonDown(Button_Z))
		{
			for (u8 numSteps = 0; numSteps < (ButtonDown(Button_Z) ? 5 : 1); numSteps++)
			{
				if (test->doingGJK2D)
				{
					i32 result = GJK2D_Step(&test->gjk2D, GJK2D_SupportCircleRect, &test->circle, &test->rectangle);
					test->gjk2DStep++;
					if (result > 0)
					{
						test->doingGJK2D = false;
						test->finishedGJK2D = true;
						test->foundOverlapGJK2D = true;
					}
					else if (result < 0)
					{
						test->doingGJK2D = false;
						test->finishedGJK2D = true;
						test->foundOverlapGJK2D = false;
					}
				}
				else if (!test->finishedGJK2D || ButtonPressed(Button_Enter))
				{
					test->doingGJK2D = true;
					test->finishedGJK2D = false;
					test->foundOverlapGJK2D = false;
					test->gjk2D.numVerts = 0;
					test->gjk2DStep = 0;
				}
			}
		}
		if (ButtonPressed(Button_Backspace))
		{
			if (test->doingGJK2D)
			{
				test->doingGJK2D = false;
				test->finishedGJK2D = false;
				test->foundOverlapGJK2D = false;
			}
		}
		
		if (ButtonPressed(Button_Tilde))
		{
			test->snapToGrid = !test->snapToGrid;
		}
		
		if (ButtonPressed(Button_1))
		{
			test->showFlowers = !test->showFlowers;
		}
		if (ButtonPressed(Button_2))
		{
			test->showCircleRec = !test->showCircleRec;
		}
		if (ButtonPressed(Button_3))
		{
			test->showRecPoly = !test->showRecPoly;
		}
		if (ButtonPressed(Button_4))
		{
			test->showCirclePoly = !test->showCirclePoly;
		}
		
		test->gjkResult = GJKCircleVsRec(&test->circle, &test->rectangle);
	}
	
	// +--------------------------------------------------------------+
	// |                            Render                            |
	// +--------------------------------------------------------------+
	{
		RcBegin(&app->defaultShader, &app->defaultFont, NewRec(Vec2_Zero, RenderScreenSize));
		RcClearColorBuffer(NewColor(Color_Black));
		RcClearDepthBuffer(1.0f);
		RcBindNewFont(&app->newFont);
		RcSetFontSize(24);
		RcSetViewMatrix(Mat4Translate(NewVec3(-test->viewPos.x, -test->viewPos.y, 0)));
		v2 mousePos = RenderMousePos + test->viewPos;
		
		rec viewRec = NewRec(test->viewPos, RenderScreenSize);
		
		// +==============================+
		// |       Draw Grid Lines        |
		// +==============================+
		{
			r32 startX = viewRec.x;
			startX = (r32)CeilR32(startX/gridSize) * gridSize;
			for (u32 x = 0; x < viewRec.width/gridSize; x++)
			{
				r32 xPos = startX + (r32)x*gridSize;
				Color_t color = ColorTransparent(NewColor(Color_Green), 0.5f);
				r32 thickness = 1;
				if (AbsR32(xPos) < gridSize/2)
				{
					color = NewColor(Color_Green);
					thickness = 3;
				}
				RcDrawLine(NewVec2(xPos, viewRec.y), NewVec2(xPos, viewRec.y + viewRec.height), thickness, color);
			}
			
			r32 startY = viewRec.y;
			startY = (r32)CeilR32(startY/gridSize) * gridSize;
			for (u32 y = 0; y < viewRec.height/gridSize; y++)
			{
				r32 yPos = startY + (r32)y*gridSize;
				Color_t color = ColorTransparent(NewColor(Color_Red), 0.4f);
				r32 thickness = 1;
				if (AbsR32(yPos) < gridSize/2)
				{
					color = NewColor(Color_Red);
					thickness = 3;
				}
				RcDrawLine(NewVec2(viewRec.x, yPos), NewVec2(viewRec.x + viewRec.width, yPos), thickness, color);
			}
		}
		
		// +==============================+
		// |         Draw Shapes          |
		// +==============================+
		RcDrawCircle(test->circle.center, test->circle.radius, NewColor(test->gjkResult ? Color_White : Color_Gray));
		RcDrawRectangle(test->rectangle, NewColor(test->gjkResult ? Color_White : Color_Gray));
		for (u32 vIndex = 0; vIndex < test->poly.numVerts; vIndex++)
		{
			v2 p1 = test->poly.verts[vIndex];
			v2 p2 = test->poly.verts[(vIndex+1) % test->poly.numVerts];
			RcDrawLine(p1, p2, 1, NewColor(Color_White));
		}
		
		// +==============================+
		// |    Draw Sums/Subtractions    |
		// +==============================+
		v2 polyCenter = GetPolyCenter(&test->poly);
		v2 recCenter = RecCenter(test->rectangle);
		v2 circleCenter = test->circle.center;
		u32 numSupports = 75;
		v2 lastSumPoints[3]; ClearArray(lastSumPoints);
		v2 lastDiffPoints[3]; ClearArray(lastDiffPoints);
		for (u32 sIndex = 0; sIndex <= numSupports; sIndex++)
		{
			r32 direction = sIndex * (Pi32*2 / (r32)numSupports);// + Pi32/4;
			v2 dirVec = NewVec2(CosR32(direction), SinR32(direction));
			
			v2 circleSupport = GetCircleSupport(test->circle, dirVec);
			v2 circleSupportOpposite = GetCircleSupport(test->circle, -dirVec);
			if (test->showFlowers)
			{
				RcDrawQuadCurve(circleCenter, circleCenter + dirVec * 100, circleSupport, 5, 1, NewColor(Color_White));
			}
			
			v2 recSupport = GetRectSupport(test->rectangle, dirVec);
			v2 recSupportOpposite = GetRectSupport(test->rectangle, -dirVec);
			if (test->showFlowers)
			{
				RcDrawQuadCurve(recCenter, recCenter + dirVec * 100, recSupport, 5, 1, NewColor(Color_White));
			}
			
			v2 polySupport = GetPolySupport(&test->poly, dirVec);
			v2 polySupportOpposite = GetPolySupport(&test->poly, -dirVec);
			if (test->showFlowers)
			{
				RcDrawQuadCurve(polyCenter, polyCenter + dirVec * 100, polySupport, 5, 1, NewColor(Color_White));
			}
			
			v2 sumCenters[3];
			sumCenters[0] = circleCenter + recCenter;
			sumCenters[1] = recCenter + polyCenter;
			sumCenters[2] = circleCenter + polyCenter;
			
			v2 diffCenters[3];
			diffCenters[0] = circleCenter - recCenter;
			diffCenters[1] = recCenter - polyCenter;
			diffCenters[2] = circleCenter - polyCenter;
			
			v2 sumSupports[3];
			sumSupports[0] = circleSupport + recSupport;
			sumSupports[1] = recSupport + polySupport;
			sumSupports[2] = circleSupport + polySupport;
			
			v2 diffSupports[3];
			diffSupports[0] = circleSupport - recSupportOpposite;
			diffSupports[1] = recSupport - polySupportOpposite;
			diffSupports[2] = circleSupport - polySupportOpposite;
			
			if (sIndex > 0)
			{
				if (test->showCircleRec)
				{
					RcDrawLine(lastSumPoints[0], sumSupports[0], 1, NewColor(Color_Yellow));
					RcDrawLine(lastDiffPoints[0], diffSupports[0], 1, NewColor(Color_Orange));
					if (test->showFlowers)
					{
						RcDrawQuadCurve(diffCenters[0], diffCenters[0] + dirVec * 100, diffSupports[0], 5, 1, NewColor(Color_Orange));
						RcDrawQuadCurve(sumCenters[0], sumCenters[0] + dirVec * 100, sumSupports[0], 5, 1, NewColor(Color_Yellow));
						if (test->showTangents)
						{
							v2 tangent = Vec2PerpRight(dirVec);
							RcDrawLine(sumSupports[0] + tangent*500, sumSupports[0] - tangent*500, 1, NewColor(Color_Cyan));
						}
					}
				}
				if (test->showRecPoly)
				{
					RcDrawLine(lastSumPoints[1], sumSupports[1], 1, NewColor(Color_Yellow));
					RcDrawLine(lastDiffPoints[1], diffSupports[1], 1, NewColor(Color_Orange));
					if (test->showFlowers)
					{
						RcDrawQuadCurve(diffCenters[1], diffCenters[1] + dirVec * 100, diffSupports[1], 5, 1, NewColor(Color_Orange));
						RcDrawQuadCurve(sumCenters[1], sumCenters[1] + dirVec * 100, sumSupports[1], 5, 1, NewColor(Color_Yellow));
						if (test->showTangents)
						{
							v2 tangent = Vec2PerpRight(dirVec);
							RcDrawLine(sumSupports[1] + tangent*500, sumSupports[1] - tangent*500, 1, NewColor(Color_Cyan));
						}
					}
				}
				if (test->showCirclePoly)
				{
					RcDrawLine(lastSumPoints[2], sumSupports[2], 1, NewColor(Color_Yellow));
					RcDrawLine(lastDiffPoints[2], diffSupports[2], 1, NewColor(Color_Orange));
					if (test->showFlowers)
					{
						RcDrawQuadCurve(diffCenters[2], diffCenters[2] + dirVec * 100, diffSupports[2], 5, 1, NewColor(Color_Orange));
						RcDrawQuadCurve(sumCenters[2], sumCenters[2] + dirVec * 100, sumSupports[2], 5, 1, NewColor(Color_Yellow));
						if (test->showTangents)
						{
							v2 tangent = Vec2PerpRight(dirVec);
							RcDrawLine(sumSupports[2] + tangent*500, sumSupports[2] - tangent*500, 1, NewColor(Color_Cyan));
						}
					}
				}
			}
			
			lastSumPoints[0] = sumSupports[0];
			lastSumPoints[1] = sumSupports[1];
			lastSumPoints[2] = sumSupports[2];
			lastDiffPoints[0] = diffSupports[0];
			lastDiffPoints[1] = diffSupports[1];
			lastDiffPoints[2] = diffSupports[2];
		}
		
		// {
		// 	v2 sumCenter = recCenter + polyCenter;
		// 	v2 dirVec = Vec2Normalize(mousePos - sumCenter);
		// 	v2 tangent = Vec2PerpRight(dirVec);
			
		// 	v2 circleSupport = GetCircleSupport(test->circle, dirVec);
		// 	v2 recSupport = GetRectSupport(test->rectangle, dirVec);
		// 	v2 polySupport = GetPolySupport(&test->poly, dirVec);
		// 	v2 polySupportOpposite = GetPolySupport(&test->poly, -dirVec);
		// 	v2 sumSupport = recSupport + polySupport;
		// 	v2 diffSupport = recSupport - polySupportOpposite;
		// 	RcDrawLine(circleSupport + tangent*100, circleSupport - tangent*100, 1, NewColor(Color_Cyan));
		// 	RcDrawLine(recSupport + tangent*100, recSupport - tangent*100, 1, NewColor(Color_Cyan));
		// 	RcDrawLine(polySupport + tangent*100, polySupport - tangent*100, 1, NewColor(Color_Cyan));
		// 	RcDrawLine(sumSupport + tangent*100, sumSupport - tangent*100, 1, NewColor(Color_Cyan));
		// 	RcDrawLine(diffSupport + tangent*100, diffSupport - tangent*100, 1, NewColor(Color_Cyan));
		// }
		
		// +==============================+
		// |       Draw GJK2D Info        |
		// +==============================+
		if (test->doingGJK2D || test->finishedGJK2D)
		{
			if (test->gjk2D.numVerts > 1)
			{
				Color_t color = NewColor(Color_Orange);
				if (test->finishedGJK2D)
				{
					color = NewColor(test->foundOverlapGJK2D ? Color_Green : Color_Red);
				}
				for (u32 vIndex = 0; vIndex < test->gjk2D.numVerts; vIndex++)
				{
					v2 vert = test->gjk2D.verts[vIndex];
					v2 nextVert = test->gjk2D.verts[(vIndex+1) % test->gjk2D.numVerts];
					RcDrawLine(vert, nextVert, 1, color);
				}
			}
			for (u32 vIndex = 0; vIndex < test->gjk2D.numVerts; vIndex++)
			{
				v2 vert = test->gjk2D.verts[vIndex];
				RcDrawCircle(vert, 5, NewColor(Color_Cyan));
				if (vIndex == (u32)test->gjk2D.numVerts-1 && !(test->finishedGJK2D && test->foundOverlapGJK2D))
				{
					RcDrawLineArrow(vert, vert + test->gjk2D.dirVec*15, 5, 1, NewColor(Color_Cyan));
				}
			}
			
			
			
			RcSetFontAlignment(Alignment_Right);
			v2 textPos = NewVec2(viewRec.x + viewRec.width-10, viewRec.y + RcGetLineHeight());
			
			RcNewPrintStringFull(textPos, NewColor(Color_White), 0, nullptr, "Step %u", test->gjk2DStep);
			textPos.y += RcGetLineHeight();
			
			RcNewPrintStringFull(textPos, NewColor(Color_White), 0, nullptr, "NumVerts %u", test->gjk2D.numVerts);
			textPos.y += RcGetLineHeight();
		}
		
		// +==============================+
		// |    Draw SnapToGrid Label     |
		// +==============================+
		if (test->snapToGrid)
		{
			RcSetFontAlignment(Alignment_Right);
			v2 textPos = NewVec2(viewRec.x + viewRec.width-10, viewRec.y + viewRec.height - (10 + RcGetMaxExtendDown()));
			RcNewDrawNtString("Snap", textPos, NewColor(Color_White));
		}
	}
	
	return newState;
}
