/*
File:   tester.h
Author: Taylor Robbins
Date:   11\09\2018
*/

#ifndef _TESTER_H
#define _TESTER_H

struct TesterData_t
{
	bool initialized;
	
	v2 viewPos;
	
	Circle_t circle;
	rec rectangle;
	Polygon_t poly;
	
	u32 vertIndex;
	
	bool doingGJK2D;
	bool finishedGJK2D;
	bool foundOverlapGJK2D;
	GJK2D_t gjk2D;
	u32 gjk2DStep;
	
	bool gjkResult;
	
	bool snapToGrid;
	bool showFlowers;
	bool showTangents;
	bool showCircleRec;
	bool showRecPoly;
	bool showCirclePoly;
};

#endif //  _TESTER_H
